use chrono::{Duration, NaiveTime, Timelike};
use std::fmt;

#[derive(Debug, PartialEq)]
pub struct Clock {
    hours: String,
    minutes: String,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let midnight = NaiveTime::from_hms(0, 0, 0);
        let minutes_from = (hours * 60) + minutes;
        let time = midnight + Duration::minutes(minutes_from as i64);
        Clock {
            hours: format!("{:02}", time.hour()),
            minutes: format!("{:02}", time.minute()),
        }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        let num_hours: i32 = self.hours.parse().unwrap();
        let num_mins: i32 = self.minutes.parse().unwrap();
        let time = NaiveTime::from_hms(num_hours as u32, num_mins as u32, 0 as u32)
            + Duration::minutes(minutes as i64);
        Clock {
            hours: format!("{:02}", time.hour()),
            minutes: format!("{:02}", time.minute()),
        }
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.hours, self.minutes)
    }
}
