pub fn raindrops(n: u32) -> String {
    let mut res: String = String::from("");
    if n % 3 == 0 {
        res.push_str("Pling")
    };
    if n % 5 == 0 {
        res.push_str("Plang")
    };
    if n % 7 == 0 {
        res.push_str("Plong")
    };
    let str_n = n.to_string();
    if res.len() == 0 {
        res.push_str(&str_n)
    }
    res
}

pub fn raindrops2(n: u32) -> String {
    let mut res = String::from("");
    let mut check_factor = |factor, add| {
        if n % factor == 0 {
            res.push_str(add)
        }
    };

    check_factor(3, "Pling");
    check_factor(5, "Plang");
    check_factor(7, "Plong");

    if res.is_empty() {
        res = n.to_string()
    };
    res
}
