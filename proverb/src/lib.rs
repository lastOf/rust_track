pub fn build_proverb(list: &[&str]) -> String {
    let mut proverb: String = String::new();
    if list.len() == 0 {
        return proverb;
    } else if list.len() == 1 {
        let only_last_str = format!("And all for the want of a {}.", list[0]);
        proverb.push_str(&only_last_str);
        return proverb;
    }
    let add_first_str = format!("For want of a {} ", list[0]);
    proverb.push_str(&add_first_str);

    for x in list[1..(list.len() - 1)].iter() {
        let add_text: String = format!(
            "the {word} was lost.
For want of a {word} ",
            word = x
        );
        let str_text = &add_text[..];
        proverb.push_str(str_text);
    }

    let add_last_str = format!(
        "the {} was lost.
And all for the want of a {}.",
        list[list.len() - 1],
        list[0]
    );
    proverb.push_str(&add_last_str);

    proverb
}
