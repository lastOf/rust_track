use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let lowercase_word = word.to_lowercase();

    let mut results = HashSet::new();

    let try_copy = possible_anagrams.to_owned();

    fn sort(s: &str) -> String {
        let mut temp = s.to_lowercase().chars().collect::<Vec<char>>();
        temp.sort();
        temp.into_iter().collect::<String>()
    }

    for el in try_copy {
        if el.to_lowercase() == lowercase_word {
        } else {
            if sort(&lowercase_word) == sort(&el) {
                results.insert(el);
            }
        }
    }
    results
}
